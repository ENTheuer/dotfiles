#
# ~/.bashrc
#

# If not running interactively, don't do anything

[[ $- != *i* ]] && return


# Terminal Startup
neofetch | lolcat
# VI-Mode
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# Extracting archives
function ex () {
	if [ -f $1 ]; then
		case $1 in
			*.tar.bz2)	tar xjf $1		;;
			*.tar.gz)	tar xzf $1		;;
			*.tar.xz)	tar xf	$1		;;
			*.bz2)		bunzip2	$1		;;
			*.rar)		unrar x	$1		;;
			*.gz)		gunzip	$1		;;
			*.tar)		tar xf	$1		;;
			*.tbz2)		tar	xjf	$1		;;
			*.tgz)		tar xzf	$1		;;
			*.zip)		unzip	$1		;;
			*.Z)		uncompress $1	;;
			*.7z)		7z x	$1		;;
			*)			echo "'$1' cannot be extracted via ex()"	;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

function resEmacs () {
	killall emacs
	/usr/bin/emacs --daemon & disown
}

# Exports
export PAGER=bat
export EDITOR=nvim
export VISUAL="emacsclient -c -a 'emacs'"
export -f ex
export -f resEmacs

# Shopt
shopt -s autocd


# Listing stuff
alias ls='exa --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -lah --color=always --group-directories-first'
alias l.='exa -a | grep "^\."'

#Make things readable
alias top=htop
alias df='df -h'
alias grep='grep --color=always'
alias dffs='df | grep /dev/sd'


# Memes
alias look-i-am-cool='screenfetch | lolcat && neofetch | lolcat && fortune | cowsay | lolcat'
alias neofetch='neofetch | lolcat'
alias wanna-fuck-me-now='echo "I use Arch btw" | cowsay | lolcat'
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'


# Typo correctors
alias eixt=exit
alias exti=exit
alias xeit=exit
alias exot=exit
alias exir=exit
alias exut=exit
alias rxit=exit
alias exto=exit
alias :q=exit
alias :q!=exit
alias :wq=exit

# Package Management aliases
alias fulloff='sudo -- pacman -Syu'              	# Updates packages from official Repos
alias fullaur='paru -Sua'                        	# Updates packages from the AUR
alias fullsys='paru -Syu'                        	# Updates everything on the system   
alias remorph='sudo -- pacman -Rns $(pacman -Qdtq)'	# Removes orphaned packages
alias install='sudo -- pacman -S'
alias search='sudo -- pacman -Ss'
alias WEG='sudo -- pacman -Rnsc'					# !USE WITH CAUTION!
alias yeet='sudo -- pacman -Rnsc --no-confirm'		# You are dumb, aren't you?
alias unlock="sudo -- rm -f /var/lib/pacman/db.lck" # don't kill your System with that!

# Emacs Aliases
alias emacs='emacsclient -c -a "emacs"'
alias emanw='/usr/bin/emacs -nw'
alias doom=.emacs.d/bin/doom
alias vim=nvim

# Git Aliases
alias gadd="git add"
alias gcommit="git commit"
alias gpush="git push"

# PAMixer
alias masvol="pamixer --get-volume-human"

# Moving through the Filesystem
alias ...=../../
alias ....=../../..
alias 5.=../../../../

# Prompt
eval "$(starship init bash)"
