#!/usr/bin/env bash
/usr/bin/lxpolkit &
/usr/bin/emacs --daemon &
/usr/lib/kdeconnectd &
adb start-server &
flameshot &
/usr/lib/notification-daemon-1.0/notification-daemon &
deadd-notification-center &
picom --experimental-backend &
polychromatic-cli --effect Vertical\ TransTrans &
python /home/emma/.config/qtile/Infloop.py &
