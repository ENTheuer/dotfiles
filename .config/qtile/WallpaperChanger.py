### Import required Libraries
from datetime import datetime # Get the Hours OwO
from os import system # Execute Shell commands (Required for feh)

### Define The current hour
Time = datetime.now()
CurrentHour = Time.hour

# Deepnight Wallpaper
if CurrentHour in range(1, 5):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/589406")
# Morning Wallpaper
elif CurrentHour in range(6, 9):
    system("feh --bg-scale --no-fehbg /home/emmma/Bilder/Hintergründe/WaldMorgens.jpg")
# Daytime Wallpaper
elif CurrentHour in range(10, 17):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/WaldTags.jpg")
# Evening Wallpaper
elif CurrentHour in range(18, 21):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/FujiAbends.jpg")
# Early Night Wallpaper
else:
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/FujiAbendsNacht.jpg")
