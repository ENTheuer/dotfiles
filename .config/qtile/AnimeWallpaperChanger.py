### Import required Libraries
from datetime import datetime # Get the Hours OwO
from os import system # Execute Shell commands (Required for feh)

### Define The current hour
Time = datetime.now()
CurrentHour = Time.hour

# Deepnight Wallpaper
if CurrentHour in range(1, 5):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/Anime\ Wallpaper/Original\ Nightlake.jpg")
# Morning Wallpaper
elif CurrentHour in range(6, 9):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/Anime\ Wallpaper/Haus\ Morgens.jpg")
# Daytime Wallpaper
elif CurrentHour in range(10, 17):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/Anime\ Wallpaper/Street\ Tags.jpg")
# Evening Wallpaper
elif CurrentHour in range(18, 21):
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/Anime\ Wallpaper/DBall\ Wallpaper.jpg")
# Early Night Wallpaper
else:
    system("feh --bg-scale --no-fehbg /home/emma/Bilder/Hintergründe/Anime\ Wallpaper/Anime\ Temple\ Evening\ Night.jpg")
