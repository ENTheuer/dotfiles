### Import required Libraries
from os import system as execute
from random import sample
from time import sleep

# Define That Wallpapersets (A = Anime Wallpapers, N = Nature Wallpapers)
WallpaperArray = ["A", "N"]

# Randomly choose what Wallpaperset to use
Wallpaperset = sample(WallpaperArray, 1)

# Use the Selected Wallpaper in an infinte Loop bc I'm too dumb to use cronjobs lol
if Wallpaperset == ['A']:
    while True:
        execute("bash /home/emma/.config/qtile/AnimeWallpaperChanger.sh")
        sleep(60)

elif Wallpaperset == ['N']:
    while True:
        execute("bash /home/emma/.config/qtile/WallpaperChanger.sh")
        sleep(60)
